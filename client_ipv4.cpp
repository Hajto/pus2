
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <arpa/inet.h>  /* inet_ntop() */
#include <cstdio>
#include <string>
#include <iostream>
#include <unistd.h>


int main(int argc, char** argv) {
    if(argc < 3) {
        std::cout<<"Wrong number of arguments, please provide <SERVER_ADDRESS> <PORT>"<<std::endl;
        return 1;
    }

    int client_socket = socket(PF_INET, SOCK_STREAM, 0);

    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = (in_port_t) atoi(argv[2]);
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    inet_pton(AF_INET, argv[1], &server_address.sin_addr);

    connect(client_socket, (struct sockaddr *) &server_address, sizeof(server_address));

    std::string message = "Message to be sent";
    sendto(client_socket, message.c_str(), message.size(), 0, (struct sockaddr *) &server_address,
           sizeof(server_address));

    char buffer[256] = {0};
    recv(client_socket, &buffer, sizeof(buffer),0);
    std::cout << "Received" << std::endl;
    std::cout << buffer << std::endl;
    close(client_socket);

    return 0;
}