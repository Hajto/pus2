#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <unistd.h>

struct sockaddr *get_from_name(const char *address, in_port_t port_no);

struct sockaddr *set_port_v4(struct sockaddr_in *ipv4, in_port_t port_no);

struct sockaddr *set_port_v6(struct sockaddr_in6 *ipv6, in_port_t port_no);

void print_address_attached_to_socket(int socket);

in_port_t get_port(struct sockaddr_storage *storage);

/*
 * Jesli adresem jest IPv4 lub mapowane IPv4 po IPv6 to będzie używany protokół IPv4
 * Jesli podać adres IPv6, np. ::1 dla localhost to używany jest IPv6e
 */

int main(int argc, char **argv) {
    if (argc < 3) {
        std::cout << "Wrong number of arguments, please provide <SERVER_ADDRESS> <PORT>" << std::endl;
        return 1;
    }
//    const char* address_string = "192.168.1.22";
//    const char *address_string = "::ffff:127.0.0.1";
//    const char *address_string = "::1";
//    const char *address_string = "127.0.0.1";
    const char *address_string = argv[1];
    struct sockaddr *server_address = get_from_name(address_string, atoi(argv[2]));
    int client_socket = socket(server_address->sa_family, SOCK_STREAM, 0);
    size_t server_adress_size = server_address->sa_family == AF_INET ? sizeof(sockaddr_in) : sizeof(sockaddr_in6);

    if (connect(client_socket, server_address, server_adress_size) == -1) {
        perror("Error connecting: ");
        return 1;
    }

    std::string message = "Message to be sent";
    if (sendto(client_socket, message.c_str(), message.size(), 0, (struct sockaddr *) &server_address,
               sizeof(server_address)) == -1) {
        perror("Error sending: ");
        return 1;
    }

    char buffer[256] = {0};
    memset(buffer, 0, sizeof(buffer));
    recv(client_socket, &buffer, sizeof(buffer), 0);
    std::cout << "Received" << std::endl;
    std::cout << buffer << std::endl;

    sleep(1);
    print_address_attached_to_socket(client_socket);
    close(client_socket);
    free(server_address);


    return 1;
}

sockaddr *get_from_name(const char *address, in_port_t port_no) {
    struct addrinfo hints, *info;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags |= AI_CANONNAME;

    /*
        ai_family   This field specifies the desired address family for the
                   returned addresses.  Valid values for this field include
                   AF_INET and AF_INET6.  The value AF_UNSPEC indicates that
                   getaddrinfo() should return socket addresses for any
                   address family (either IPv4 or IPv6, for example) that
                   can be used with node and service.

        ai_socktype This field specifies the preferred socket type, for exam‐
                   ple SOCK_STREAM or SOCK_DGRAM.  Specifying 0 in this
                   field indicates that socket addresses of any type can be
                   returned by getaddrinfo().

        ai_protocol This field specifies the protocol for the returned socket
                   addresses.  Specifying 0 in this field indicates that
                   socket addresses with any protocol can be returned by
                   getaddrinfo().

        ai_flags    This field specifies additional options, described below.
                   Multiple flags are specified by bitwise OR-ing them
                   together.
     */

    int return_code = getaddrinfo(address, nullptr, &hints, &info);
    if (return_code != 0) {
        perror("Error parsing address");
        return nullptr;
    }

    struct sockaddr *ptr = (sockaddr*) malloc(info->ai_addrlen);
    memcpy(ptr, info->ai_addr, info->ai_addrlen);

    switch (info->ai_family) {
        case AF_INET:
            ptr = set_port_v4((struct sockaddr_in *) ptr, port_no);
            break;

        case AF_INET6:
            ptr = set_port_v6((struct sockaddr_in6 *) ptr, port_no);
            break;

        default:
            return NULL;
    }
    freeaddrinfo(info);

    return ptr;
}

void print_address_attached_to_socket(int socket) {
    socklen_t socket_length = sizeof(sockaddr_storage);
    struct sockaddr_storage storage;
    memset(&storage, 0, sizeof(sockaddr_storage));
    int return_code = getsockname(socket, (struct sockaddr *) &storage, &socket_length);
    if (return_code != 0) {
        perror("Errror getting sock name");
        exit(1);
    }
    if (storage.ss_family == 0) {
        std::cout << "Family is unspecified getsockname failed without telling why..." << std::endl;
        return;
    } else {
        struct sockaddr *sock_addr = (struct sockaddr *) &storage;
        char host[128] = {0}, serv[128] = {0};
        return_code = getnameinfo(sock_addr, socket_length, host, 128, serv, 128, NI_NUMERICHOST);
        if (return_code != 0) {
            printf("Errror getting resolving name");
            exit(1);
        }
        std::cout << "Host: " << host << ":" << get_port(&storage) << std::endl;
    }

}

struct sockaddr *set_port_v4(struct sockaddr_in *ipv4, in_port_t port_no) {
    ipv4->sin_port = port_no;
    return (struct sockaddr *) ipv4;
}

struct sockaddr *set_port_v6(struct sockaddr_in6 *ipv6, in_port_t port_no) {
    ipv6->sin6_port = port_no;
    return (struct sockaddr *) ipv6;
}

in_port_t get_port(struct sockaddr_storage *storage) {
    if (storage->ss_family == AF_INET) return ((struct sockaddr_in *) storage)->sin_port;
    else if (storage->ss_family == AF_INET6) return ((struct sockaddr_in6 *) storage)->sin6_port;
    else return 0;
}

