
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <arpa/inet.h>  /* inet_ntop() */
#include <cstdio>
#include <string>
#include <iostream>
#include <net/if.h>
#include <unistd.h>

int main(int argc, char** argv) {
    if(argc < 4) {
        std::cout<<"Wrong number of arguments, please provide <SERVER_ADDRESS> <PORT> <INTERFACE_ID>"<<std::endl;
        return 1;
    }
    int interface_id = if_nametoindex(argv[3]);
    if(interface_id == 0){
        perror("Error converting interface name to id:");
        return 1;
    }

    int client_socket = socket(PF_INET6, SOCK_STREAM, 0);

    struct sockaddr_in6 server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin6_family = AF_INET6;
    server_address.sin6_port = (in_port_t) atoi(argv[2]);
    server_address.sin6_scope_id = interface_id;
    inet_pton(AF_INET6, argv[1], &server_address.sin6_addr);

    if(connect(client_socket, (struct sockaddr *) &server_address, sizeof(server_address)) == -1){
        perror("Error connecting: ");
        return 1;
    }

    std::string message = "Message to be sent";
    if(sendto(client_socket, message.c_str(), message.size(), 0, (struct sockaddr *) &server_address,
           sizeof(server_address)) == -1 ){
        perror("Error sending: ");
        return 1;
    }

    char buffer[256] = {0};
    recv(client_socket, &buffer, sizeof(buffer),0);
    std::cout << "Received" << std::endl;
    std::cout << buffer << std::endl;
    close(client_socket);

    return 0;
}
