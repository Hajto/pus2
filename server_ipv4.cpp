#include <sys/socket.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>

/*
 * Odpowiedzi:
 * a) Tak istnieje komunikacja między klientem IPv4 i serwerem IPv6
 * b) Serwer musi wspierać mapowanie adresów IPv4 na adresy IPv6
 * c) Datagramy mają postać ramek pakietów IPv4 (co widać na zrzucie ekranuw z wiresharka)
 */

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Wrong number of arguments, please provide <PORT>" << std::endl;
        return 1;
    }

    int server_socket = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_port = atoi(argv[1]);
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address)) == -1) {
        std::cout << "Socket already bound" << std::endl;
        perror("Blad: ");
        return 1;
    }

    listen(server_socket, 100);

    while (true) {
        struct sockaddr_in client_address;
        socklen_t client_length;
        memset(&client_address, 0, sizeof(client_address));
        int client = accept(server_socket, (struct sockaddr *) &client_address, &client_length);
        char address_buffer[INET6_ADDRSTRLEN] = {0};
        inet_ntop(AF_INET, &(client_address.sin_addr), address_buffer, INET_ADDRSTRLEN);
        std::cout << "Client addres: " << address_buffer << ":" << client_address.sin_port << std::endl;

        char buffer[256] = {0};
        recv(server_socket, &buffer, sizeof(buffer), 0);

        char message[] = "Laboratorium PUS";
        send(client, message, sizeof(message), 0);
        close(client);
    }
}